#include <sqlite3.h>
#include <stddef.h>
#include <stdio.h>
#include <mongoose.h>

/// Creates a database with empty tables, then populate the City table with
/// content from cities.txt .
/// This function is called from the command-line with './travelexpress create-db'.
void createDatabase() {
    sqlite3* db;
    if (sqlite3_open("travelexpress.sqlite3", &db) == SQLITE_OK) {
        // Create tables.
        int rc = sqlite3_exec(db,
            "CREATE TABLE City(id INTEGER PRIMARY KEY, name TEXT);"
            "CREATE TABLE User(id INTEGER PRIMARY KEY, username TEXT, "
                "email TEXT, sha3password BLOB, phone TEXT, "
                "comment TEXT DEFAULT '', credits INTEGER DEFAULT 0);"
            "CREATE TABLE Session(id BLOB PRIMARY KEY, userid INTEGER, "
                "creation INTEGER, FOREIGN KEY(userid) REFERENCES User(id));"
            "CREATE TABLE Travel(id INTEGER PRIMARY KEY, userid INTEGER, "
                "from_ INTEGER, dest INTEGER, passengers INTEGER, "
                "hour INTEGER, return INTEGER, punctual TEXT, regular TEXT, "
                "FOREIGN KEY(userid) REFERENCES User(id), "
                "FOREIGN KEY(from_) REFERENCES City(id), "
                "FOREIGN KEY(dest) REFERENCES City(id));"
        , NULL, NULL, NULL);
        if (rc) puts(sqlite3_errmsg(db));
        
        // Putting all cities. Insertion is relatively slow,
        // so we print messages to ensure user is aware that the program is working.
        puts("Loading all cities in database, may take some time...");

        char* cities_data = mg_file_read("data/cities.txt", NULL);
        char const* cursor = cities_data;

        // Supposing all city names "cityname, province" are under 200 characters, else truncating.
        char name_buffer[200]; 
        // Preparing SQL query with placeholders '?' to be bound in the loop.
        sqlite3_stmt* stmt;
        rc = sqlite3_prepare_v2(db, "INSERT INTO City(id, name) VALUES (?,?);", -1, &stmt, NULL);
        if (rc) puts(sqlite3_errmsg(db));

        for(int i = 1; ; ++i) {
            if (i % 256 == 0) printf("\n    %d cities done...", i);

            int nb_read = 0;
            unsigned value = 0;
            // Parse 'index: cityname\n' with sscanf.
            // %199[^\n] means up to 199 characters, stopping at '\n'.
            // A 200-th char can be written for null terminator. 
            sscanf(cursor, "%u: %199[^\n]\n%n", &value, name_buffer, &nb_read);
            if (nb_read == 0)
                break; // error with sscanf
            cursor += nb_read;
            // Binding index and cityname to the SQL query.
            sqlite3_bind_int(stmt, 1, value);
            sqlite3_bind_text(stmt, 2, name_buffer, -1, SQLITE_STATIC);
            // Run the query.
            sqlite3_step(stmt);
            // Reset bindings of the query.
            sqlite3_reset(stmt);
        }

        puts("\nCities all loaded!");

        free(cities_data);
        sqlite3_finalize(stmt);
    }
    
    sqlite3_close(db);
}