#include "api.h"
#include "mongoose.h"

#include <sha3.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdarg.h>

#define MAX_USERNAME_SIZE 30
#define API_SIGNIN_FORMAT_ERROR "{ \"error\": \"Le corps de /api/signin doit être [email]\\n[password]\\n[username]\\n[phone]\\n.\" }"
#define API_LOGIN_FORMAT_ERROR  "{ \"error\": \"Le corps de /api/login doit être [email]\\n[password]\\n.\"}"
#define SessionTokenSize 32

/// Parse lines of request body, expecting a given number of lines.
static bool parseBody(struct mg_str body, int expected_nb, ... /* one mg_str* per line */) {
    // Get all newline positions, return 'false' if number different from 'expected_nb'.
    int nb_newlines = 0;
    size_t newlines[expected_nb];
    for (size_t i = 0; i < body.len; ++i)
        if (body.ptr[i] == '\n') {
            if (nb_newlines >= expected_nb) {
                return false;
            }
            newlines[nb_newlines++] = i;
        }
    if (nb_newlines < expected_nb) {
        return false;
    }

    // Iterate over '...' arguments (as mg_str* ptr).
    va_list va;
    va_start(va, expected_nb);
    // First line
    struct mg_str* line = va_arg(va, struct mg_str*);
    line->ptr = body.ptr; line->len = newlines[0];
    // Other lines
    for (int i = 1; i < nb_newlines; ++i) {
        line = va_arg(va, struct mg_str*);
        line->ptr = body.ptr + newlines[i-1] + 1;
        line->len = newlines[i] - newlines[i-1] - 1;
    }
    va_end(va);
    return true;
}


/// Create a session for the given user.
static bool createSession(struct mg_str email, struct mg_str password, sqlite3* db,
                          int64_t* userid, char* token)
{
    // Hashing password.
    char sha3password[64];
    sha3_HashBuffer(512, SHA3_FLAGS_NONE, password.ptr, password.len, sha3password, 64);

    // Find user with given email and hashed password
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2(db, "SELECT id FROM user WHERE email=? AND sha3password=?;", -1, &stmt, NULL);
    if (rc) puts(sqlite3_errmsg(db));
    sqlite3_bind_text(stmt, 1, email.ptr, email.len, SQLITE_STATIC);
    sqlite3_bind_blob(stmt, 2, sha3password, 64, SQLITE_STATIC);
    if (sqlite3_step(stmt) != SQLITE_ROW) {
        // Did not find a user, return false.
        sqlite3_finalize(stmt);
        return false;
    }
    *userid = sqlite3_column_int64(stmt, 0);
    sqlite3_finalize(stmt);
    
    // Generate a random ID for the session, ensuring the ID is not already attributed.
    rc = sqlite3_prepare_v2(db, "SELECT id FROM session WHERE id=?;", -1, &stmt, NULL);
    if (rc) puts(sqlite3_errmsg(db));
    for(;;) {
        // Actual token is a base64 encoded string of a random bit sequence.
        unsigned char raw_token[SessionTokenSize * 6 / 8];
        sqlite3_randomness(sizeof(raw_token), raw_token);
        int sz = mg_base64_encode(raw_token, sizeof(raw_token), token);
        sqlite3_bind_blob(stmt, 1, token, SessionTokenSize, SQLITE_STATIC);
        if (sqlite3_step(stmt) == SQLITE_DONE)
            break; // We have found an ID which is not already allocated, break.
        sqlite3_reset(stmt);
    }
    sqlite3_finalize(stmt);
    
    int64_t creation = time(NULL);
    
    rc = sqlite3_prepare_v2(db, "INSERT INTO session(id, userid, creation) VALUES (?,?,?);", -1, &stmt, NULL);
    if (rc) puts(sqlite3_errmsg(db));
    sqlite3_bind_blob(stmt, 1, token, SessionTokenSize, SQLITE_STATIC);
    sqlite3_bind_int64(stmt, 2, *userid);
    sqlite3_bind_int64(stmt, 3, creation);
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    return true;
}

/// Signs in a new user, with an email, a password, a username and a phone number.
/// Returns a JSON object representing the user: { id, username, credits, phone, comment, token}.
/// 'token' is a base-64 identifier which needs to be given back for authentification.
/// API:
///    [POST] /api/signin : [email]\n[password]\n[username]\n[phone]\n
void api_signin(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db) {

    // Parsing request body, expecting 4 lines.
    
    struct mg_str email, password, username, phone;
    if (!parseBody(msg->body, 4, &email, &password, &username, &phone)) {
        mg_http_reply(c, 400, HEADER_JSON, API_SIGNIN_FORMAT_ERROR);
        return;
    }

    // Validating data.

    if (username.len < 3 || username.len > MAX_USERNAME_SIZE) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\":\"Le nom d'utilisateur "
            "doit avoir entre 3 et %i caractères.\"}", MAX_USERNAME_SIZE);
        return;
    }
    if (phone.len < 4) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Le numéro de téléphone "
            "doit avoir au moins 4 caractères.\"}");
        return;
    }

    for (int i = 0; i < username.len; ++i) {
        unsigned char ch = username.ptr[i];
        if (ch < 128 && !isalnum(ch) && ch != ' ' && ch != '_' && ch != '-') {
            mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Le nom d'utilisateur doit être composé de lettres et de chiffres.\"}");
            return;
        }
    }
    for (int i = 0; i < phone.len; ++i) {
        unsigned char ch = phone.ptr[i];
        if (!isdigit(ch) && ch != ' ' && ch != '+' && ch != '-') {
            mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Caractère invalide dans le numéro de téléphone: '%c'.\"}", ch);
            return;
        }
    }

    // Check unicity of given email.

    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2(db, "SELECT email FROM User WHERE email=?", -1, &stmt, NULL);
    if (rc) puts(sqlite3_errmsg(db));
    sqlite3_bind_text(stmt, 1, email.ptr, email.len, SQLITE_STATIC);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        // Found user with same email, reject signin.
        sqlite3_finalize(stmt);
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Ce courriel est déjà utilisé...\"}");
        return;
    }
    sqlite3_finalize(stmt);

    // Hash password.
    
    char sha3password[64];
    sha3_HashBuffer(512, SHA3_FLAGS_NONE, password.ptr, password.len, sha3password, 64);

    // Insert the new account in the database.
    
    rc = sqlite3_prepare_v2(db, "INSERT INTO User(email, sha3password, username, phone) VALUES (?,?,?,?);", -1, &stmt, NULL);
    if (rc) puts(sqlite3_errmsg(db));
    sqlite3_bind_text(stmt, 1, email.ptr, email.len, SQLITE_STATIC);
    sqlite3_bind_blob(stmt, 2, sha3password, sizeof(sha3password), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, username.ptr, username.len, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 4, phone.ptr, phone.len, SQLITE_STATIC);
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Erreur du serveur.\"}");
        fputs(sqlite3_errmsg(db), stderr);
        sqlite3_finalize(stmt);
        return;
    }
    sqlite3_finalize(stmt);

    // Create a session for the new account.

    int64_t userid;
    char sessionToken[SessionTokenSize+1]; // Space for null-terminator.
    if (!createSession(email, password, db, &userid, sessionToken)) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Création de session impossible.\"}");
        fputs(sqlite3_errmsg(db), stderr);
        return;
    }

    // Return the information about the created user and its session token.

    mg_http_reply(c, 200, HEADER_JSON, "{"
        "\"id\": %"PRId64", \"username\": \"%.*s\", \"credits\": 0, "
        "\"phone\": \"%.*s\", \"comment\": \"\", \"token\": \"%.*s\""
    "}", userid, (int)username.len, username.ptr, (int)phone.len, phone.ptr,
         (int)SessionTokenSize, sessionToken);
}

/// Logs in an existing user with its email and password.
/// Returns a JSON object representing the user: { id, username, credits, phone, comment, token}.
/// API:
///    [POST] /api/login : [email]\n[password]\n
void api_login(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db) {
    
    // Parsing request body, expecting 2 lines.
    
    struct mg_str email, password;
    if (!parseBody(msg->body, 2, &email, &password)) {
        mg_http_reply(c, 400, HEADER_JSON, API_LOGIN_FORMAT_ERROR);
        return;
    }

    // Create a new session.

    int64_t userid;
    char sessionToken[SessionTokenSize+1]; // Space for null-terminator.
    if (!createSession(email, password, db, &userid, sessionToken)) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Création de session impossible.\"}");
        fputs(sqlite3_errmsg(db), stderr);
        return;
    }

    // Find user details.

    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2(db, "SELECT username, phone, comment FROM user WHERE id=?;", -1, &stmt, NULL);
    if (rc) puts(sqlite3_errmsg(db));
    sqlite3_bind_int64(stmt, 1, userid);
    if (sqlite3_step(stmt) != SQLITE_ROW) {
        // Did not find the user, error.
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Erreur du serveur.\"}");
        fputs(sqlite3_errmsg(db), stderr);
        sqlite3_finalize(stmt);
    }
    
    struct mg_str username = { sqlite3_column_text(stmt, 0), sqlite3_column_bytes(stmt, 0) };
    struct mg_str phone    = { sqlite3_column_text(stmt, 1), sqlite3_column_bytes(stmt, 1) };
    struct mg_str comment  = { sqlite3_column_text(stmt, 2), sqlite3_column_bytes(stmt, 2) };

    // Return the information about the logged in user and its session token.

    mg_http_reply(c, 200, HEADER_JSON, "{"
        "\"id\": %"PRId64", \"username\": \"%.*s\", \"credits\": 0, "
        "\"phone\": \"%.*s\", \"comment\": \"\", \"token\": \"%.*s\""
    "}", userid, (int)username.len, username.ptr, (int)phone.len, phone.ptr,
         (int)SessionTokenSize, sessionToken);

    sqlite3_finalize(stmt);

}


// API: /api/logout [POST: token]
void api_logout(struct mg_connection*c , struct mg_http_message* msg, sqlite3* db) {
    sqlite3_stmt* stmt;
    sqlite3_prepare_v2(db, "DELETE FROM Session WHERE id=?;", -1, &stmt, NULL);
    sqlite3_bind_blob(stmt, 1, msg->body.ptr, msg->body.len, SQLITE_STATIC);
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Destruction de session impossible.\"}");
        fputs(sqlite3_errmsg(db), stderr);
        return;
    }
    sqlite3_finalize(stmt);
    mg_http_reply(c, 200, HEADER_JSON, "{}");
}