
#include "api/api.h"


#define httpReplyError(c, num, msg) \
    mg_http_reply(c, num, "Content-Type: text/html\r\n", "<h1>"#num": "msg"</h1>")

static char* htmlPrefix;
static char* htmlSuffix;

void freeHtmlPrefixSuffix() {
    free(htmlPrefix);
    free(htmlSuffix);
}

bool initHtmlPrefixSuffix() {
    // Loading _prefix.html and _suffix.html, used for serving HTML.
    htmlPrefix = mg_file_read("www/_prefix.html", NULL);
    htmlSuffix = mg_file_read("www/_suffix.html", NULL);

    if (htmlPrefix == NULL || htmlSuffix == NULL) {
        fputs("Could not find www/_prefix.html and www/_suffix.html .\n"
              "Are you in the correct folder?\n", stderr);
        freeHtmlPrefixSuffix();
        return false;
    }
    return true;
}


void serveHttp(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db) {
    // Is it an API call?
    if (mg_http_match_uri(msg, "/api/*")) {
        // YES: we need dynamic generation.
        // Which API is it?
        char const* apiname = msg->uri.ptr + 5;
        size_t apilen = msg->uri.len - 5;
        // Delegate API processing to individual functions.
        if (apilen == 5 && memcmp(apiname, "towns", 5) == 0)
            api_towns(c, msg);
        else if (apilen == 6 && memcmp(apiname, "signin", 6) == 0)
            api_signin(c, msg, db);
        else if (apilen == 5 && memcmp(apiname, "login", 5) == 0)
            api_login(c, msg, db);
        else if (apilen == 6 && memcmp(apiname, "logout", 6) == 0)
            api_logout(c, msg, db);
        else if (apilen == 9 && memcmp(apiname, "newtravel", 9) == 0)
            api_newtravel(c, msg, db);
        else if (apilen == 7 && memcmp(apiname, "travels", 7) == 0)
            api_travels(c, msg, db);
        else if (apilen == 6 && memcmp(apiname, "search", 6) == 0)
            api_search(c, msg, db);
        else
            httpReplyError(c, 404, "Not found...");
        return;
    }
    // It is not an API call but a regular file access.
    // Get the resource path, limited to size 255 to avoid arbitrary long processing.
    size_t urilen = msg->uri.len;
    if (urilen > 255) {
        httpReplyError(c, 414, "URI too long...");
        return;
    }
    // If no path was indicated, redirect to index.
    if (urilen == 1) { // uri == "/"
        // we want the index.
        msg->uri.ptr = "/index.html";
        urilen = 11;
    }

    // Making URI a null-terminated string prefixed with "www".
    char path[260];
    memcpy(path, "www", 3);
    memcpy(path + 3, msg->uri.ptr, urilen);
    urilen += 3;
    path[urilen] = '\0';

    // Sanity check: is there any ".." in the URI, which may escape www/ folder?
    // If yes, we could serve a file outside www/ which would cause security issues.
    if (strstr(path, "..") != NULL) {
        httpReplyError(c, 404, "Not found...");
        return;
    }

    // If URI ends with ".html", custom logic.
    if (urilen >= 5 && memcmp(path + urilen - 5, ".html", 5) == 0) {
        // We want to wrap served file between htmlPrefix and htmlSuffix,
        // so headers and footers are not duplicated in every html file.
        char* body = mg_file_read(path, NULL);
        if (body == NULL) {
            // Could not find the requested page.
            httpReplyError(c, 404, "Not found...");
            return;
        }
        // Reply with the concatenation of prefix, body and suffix.
        mg_http_reply(c, 200, "Content-Type: text/html\r\n",
            "%s%s%s",
            htmlPrefix, body, htmlSuffix
        );
        free(body);
        return;
    }

    // Else, serve static files normally.
    // These are images, CSS and JS.
    struct mg_http_serve_opts opts = {.root_dir = "www"};
    mg_http_serve_file(c, msg, path, &opts);
}