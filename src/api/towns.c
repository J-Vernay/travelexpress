#include "api.h"
#include "mongoose.h"

static const struct mg_str query_cache = {"cache=", 6};

/// Retrieves towns and provinces. Serves the 'data/cities.json' file
/// but with a cache system: if the client specify a cache time,
/// it will be checked with the file's modification time, and if the
/// cache time is higher, an empty JSON object is returned {} instead.
/// API:
///    [GET] /api/towns
///       JSON object with { province: { city: id } }
///    [GET] /api/towns?cache={unix_time} 
///       Like /api/towns, but "{}" instead if cache time is higher than local modification time.
void api_towns(struct mg_connection* c, struct mg_http_message* msg) {

    // Find if ?cache= was provided.
    char const* cache_param = mg_strstr(msg->query, query_cache);
    if (cache_param != NULL) {
        // YES: compare provided cache time to local file time.
        long long cachetime = atoll(cache_param + query_cache.len);
        time_t filetime = 0;
        mg_fs_posix.stat("data/cities.json", NULL, &filetime);
        if (cachetime > filetime) {
            // File not modified since cachetime, return empty dictionnary.
            mg_http_reply(c, 200, HEADER_JSON, "{}");
            return;
        }
    }
    // Else, no cachetime provided or filetime higher than cachetime, serve file.
    struct mg_http_serve_opts opts = { 0 };
    mg_http_serve_file(c, msg, "data/cities.json", &opts);
}