/// This file defines all API exposed to client.
/// Important requests modifying database use POST and a user connection,
/// while read-only is performed with GET methods.
/// Every response is valid JSON, which in case of error,
/// will be a dictionnary with an "error" field and a user-friendly message.

#ifndef TRAVELEXPRESS_API
#define TRAVELEXPRESS_API

#include <mongoose.h>
#include <sqlite3.h>

#define HEADER_JSON "Content-Type: application/json\r\n" 

/// Retrieves towns and provinces. Serves the 'data/cities.json' file
/// but with a cache system: if the client specify a cache time,
/// it will be checked with the file's modification time, and if the
/// cache time is higher, an empty JSON object is returned {} instead.
/// API:
///    [GET] /api/towns
///       JSON object with { province: { city: id } }
///    [GET] /api/towns?cache={unix_time} 
///       Like /api/towns, but "{}" instead if cache time is higher than local modification time.
void api_towns(struct mg_connection* c, struct mg_http_message* msg);

/// Signs in a new user, with an email, a password, a username and a phone number.
/// Returns a JSON object representing the user: { id, username, credits, phone, comment, token}.
/// 'token' is a base-64 identifier which needs to be given back for authentification.
/// API:
///    [POST] /api/signin : [email]\n[password]\n[username]\n[phone]\n
void api_signin(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db);

/// Logs in an existing user with its email and password.
/// Returns a JSON object representing the user: { id, username, credits, phone, comment, token}.
/// API:
///    [POST] /api/login : [email]\n[password]\n
void api_login(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db);

/// Logs out an existing user, invalidating its session token.
/// API:
///    [POST] /api/logout : [token]
void api_logout(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db);

/// Creates a new travel with the given informations:
/// API:
///    [POST] /api/newtravel?from={id}&dest={id}&passengers={nb}
///               &punctual={yyyy-MM-dd} # if punctual travel, one-time
///               &regular={xx-x--x}     # if regular travel, each 'x' is a day of week when the travel is occuring.
///               &hour={HHmm}
///               &return={HHmm}         # if this is a roundtrip travel
///           : [token]
void api_newtravel(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db);

/// Retrieves all travels made by a user.
/// Returns a JSON list of { id, from, dest, hour, [return, ] (punctual|regular), passengers, maxpassengers }
/// API:
///    [POST] /api/travels : [token]
void api_travels(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db); 

/// Retrieves a list of travels given wanted date, depart and arrival destination.
/// Returns a JSON list of { id, from, dest, hour, [return, ] (punctual|regular), passengercount, maxpassengers }
/// API:
///    [GET] /api/search?from={id}&to={id}&date={yyyy-MM-dd}
void api_search(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db);

/// Utility function to iterate over a query string of form 'A=B&C=D&E=F'.
/// Returns false when the query string is exhausted.
/// Modifies 'query' to remove the iterated value.
static inline bool query_iterate(struct mg_str* query, struct mg_str* name, struct mg_str* value) {
    char const* endquery = query->ptr + query->len;
    // Find name
    name->ptr = query->ptr;
    char const* endname = (char const*)memchr(name->ptr, '=', endquery - name->ptr);
    if (!endname) return false;
    name->len = endname - name->ptr;
    // Find value
    value->ptr = endname + 1;
    char const* endvalue = (char const*)memchr(value->ptr, '&', endquery - value->ptr);
    if (!endvalue) value->len = endquery - value->ptr;
    else           value->len = endvalue - value->ptr;
    // Update 'query'
    if (endvalue)  query->ptr = endvalue + 1; // skip '&'
    else           query->ptr = endquery;
    query->len = endquery - query->ptr;
    return true;
}

#endif