#include "api.h"
#include "mongoose.h"


/// Retrieves all travels made by a user.
/// Returns a JSON list of { id, from, dest, hour, [return, ] (punctual|regular), passengers, maxpassengers }
/// API:
///    [POST] /api/travels : [token]
void api_travels(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db) {

    // Get user with the token given in msg->body.

    sqlite3_stmt* stmt;
    sqlite3_prepare_v2(db, "SELECT userid FROM Session WHERE id = ?;", -1, &stmt, NULL);
    sqlite3_bind_blob(stmt, 1, msg->body.ptr, msg->body.len, SQLITE_STATIC);
    if (sqlite3_step(stmt) != SQLITE_ROW) {
        sqlite3_finalize(stmt);
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Session invalide.\"}");
        return;
    }    
    int64_t userid = sqlite3_column_int64(stmt, 0);
    sqlite3_finalize(stmt);

    // Construct a JSON array in 'result'.

    sqlite3_str* result = sqlite3_str_new(db);
    sqlite3_str_appendall(result, "[ ");

    // For each travel which the user owns.

    sqlite3_prepare_v2(db, "SELECT * FROM Travel WHERE userid = ?;", -1, &stmt, NULL);
    sqlite3_bind_int64(stmt, 1, userid);
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        // Travel(id, userid, from_, dest, passengers, hour, return, punctual, regular)

        sqlite3_str_appendf(result,
            "{ \"id\": %"PRId64", \"from\": %"PRId64", \"dest\": %"PRId64", \"hour\": %"PRId64", ",
            sqlite3_column_int64(stmt, 0),
            sqlite3_column_int64(stmt, 2),
            sqlite3_column_int64(stmt, 3),
            sqlite3_column_int64(stmt, 5));
        
        // Is it roundtrip = is column 6 'return' not NULL ?
        if (sqlite3_column_type(stmt, 6) == SQLITE_INTEGER)
            sqlite3_str_appendf(result, "\"return\": %"PRId64", ",
                sqlite3_column_int64(stmt, 6));
        
        // Is it punctual = is column 7 'punctual' not NULL ?
        if (sqlite3_column_type(stmt, 7) == SQLITE_TEXT)
            sqlite3_str_appendf(result, "\"punctual\": \"%.*s\", ",
                sqlite3_column_bytes(stmt, 7),
                sqlite3_column_text(stmt, 7));
        
        // Is it regular = is column 8 'regular' not NULL ?
        if (sqlite3_column_type(stmt, 8) == SQLITE_TEXT)
            sqlite3_str_appendf(result, "\"regular\": \"%.*s\", ",
                sqlite3_column_bytes(stmt, 8),
                sqlite3_column_text(stmt, 8));
        
        sqlite3_str_appendf(result, "\"maxpassengers\": %"PRId64", ",
            sqlite3_column_int64(stmt, 4));
        
        // TODO: Make database calls to get all passengers which have reserved this travel.
        sqlite3_str_appendall(result, "\"passengers\": []");
        
        sqlite3_str_appendall(result, "},");
    }
    // replacing trailing comma with ']' to end the array.
    char* result_str = sqlite3_str_value(result);
    int result_size = sqlite3_str_length(result);
    result_str[result_size-1] = ']';

    mg_http_reply(c, 200, HEADER_JSON, "%s", sqlite3_str_value(result));

    sqlite3_str_finish(result);
}