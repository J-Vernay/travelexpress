
// This object is responsible for user authentification.
// It stores on localStorage user's session token and informations to be displayed on page.

// Ensure only one definition
if (window.User == undefined) window.User = {
    _value: null,

    // Get the user data IF it is already loaded or available in localStorage.
    // Else, return 'null'. Used for example to display username on navbar if logged in.
    rawget() {
        if (this._value) return this._value;
        const userjson = localStorage.getItem("user");
        if (!userjson)  
            return null;
        this._value = JSON.parse(userjson);
        return this._value;
    },

    // Get the user data if available, else will display a popup to user
    // to ask for login or signin. May return 'null' if the user closes the popup.
    async get() {
        if (this._value) return this._value;
        const userjson = localStorage.getItem("user");
        if (!userjson)
            return await this.askLogin();
        this._value = JSON.parse(userjson);
        return this._value;
    },

    // Ask the server to login with the given email and password.
    // Return the user data or throws error if invalid credentials.
    login(email, password) {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "/api/login", true);

        return new Promise((resolve, reject) => {
            xhr.onload = () => {
                if (xhr.responseText == "{}") {
                    reject(new Error());
                    return;
                }
                const response = JSON.parse(xhr.responseText);
                if (response.error) {
                    reject(new Error(response.error));
                    return;
                }
                localStorage.setItem("user", xhr.responseText);
                this._value = response;
                resolve(response);
            };
            xhr.send(email + "\n" + password + "\n");
        });
    },

    // Ask the server to create a new account.
    // Return the user data or throws an error.
    create(email, password, username, phone) {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "/api/signin", true);

        return new Promise((resolve,reject) => {
            xhr.onload = () => {
                if (xhr.responseText == "{}") {
                    reject(new Error());
                    return;
                }
                const response = JSON.parse(xhr.responseText);
                if (response.error) {
                    reject(new Error(response.error));
                    return;
                }
                localStorage.setItem("user", xhr.responseText);
                this._value = response;
                resolve(response);
            };
            xhr.send(email + "\n" + password + "\n" + username + "\n" + phone + "\n");
        });
    },

    // Display popup to ask user for login or signin.
    async askLogin() {
        const createElem = (tagName, className, parent, innerText) => {
            const elem = document.createElement(tagName);
            elem.className = className;
            if (parent) parent.appendChild(elem);
            if (innerText) elem.innerText = innerText;
            return elem;
        };

        // Construction
        
        const modal = createElem("div", "modal is-active");
        createElem("div", "modal-background", modal);
        const card = createElem("div", "modal-card", modal);
        const header = createElem("div", "modal-card-head", card);
        createElem("p", "modal-card-title", header, "Vous n'êtes pas connecté.");
        const closeButton = createElem("button", "delete", header);

        const body = createElem("section", "modal-card-body", card);
        
        const tabs = createElem("p", "panel-tabs", body);
        const loginTab = createElem("a", "", tabs, "Se connecter");
        const signinTab = createElem("a", "", tabs, "S'inscrire");

        const form = createElem("form", "", body);

        const errorNotif = createElem("div", "notification is-danger is-light", form);
        const closeErrorButton = createElem("button", "delete", errorNotif);
        const errorText = createElem("p", "", errorNotif);
        closeErrorButton.onclick = () => { errorNotif.style.display = "none"; };
        closeErrorButton.onclick();

        const emailField = createElem("div", "field", form);
        const emailLabel = createElem("label", "label", emailField, "Courriel");
        const emailInput = createElem("input", "input", emailField);
        emailInput.type = "email"; emailInput.placeholder = "courriel@example.org";
        
        const passwordField = createElem("div", "field", form);
        const passwordLabel = createElem("label", "label", passwordField, "Mot de passe");
        const passwordInput = createElem("input", "input", passwordField);
        passwordInput.type = "password";

        const passwordConfirmField = createElem("div", "field", form);
        const passwordConfirmLabel = createElem("label", "label", passwordConfirmField, "Confirmation du mot de passe");
        const passwordConfirmInput = createElem("input", "input", passwordConfirmField);
        passwordConfirmInput.type = "password";

        const usernameField = createElem("div", "field", form);
        const usernameLabel = createElem("label", "label", usernameField, "Nom d'utilisateur");
        const usernameInput = createElem("input", "input", usernameField);

        const phoneField = createElem("div", "field", form);
        const phoneLabel = createElem("label", "label", phoneField, "Numéro de téléphone");
        const phoneInput = createElem("input", "input", phoneField);
        phoneInput.type = "tel";

        const submitButton = createElem("input", "button is-primary");
        submitButton.type = "submit";
        
        createElem("div", "field columns is-centered mt-5", form)
            .appendChild(createElem("div", "column is-4")
                .appendChild(submitButton));
        
        const pageBody = document.getElementsByTagName("body")[0];
        pageBody.appendChild(modal);

        // Event handlers for form responsiveness (disabling and enabling inputs).

        loginTab.onclick = () => {
            loginTab.classList.add("is-active");
            signinTab.classList.remove("is-active");
            passwordConfirmField.style.display = "none";
            usernameField.style.display = "none";
            phoneField.style.display = "none";
            submitButton.value = "Se connecter";
        };

        signinTab.onclick = () => {
            loginTab.classList.remove("is-active");
            signinTab.classList.add("is-active");
            passwordConfirmField.style.display = "";
            usernameField.style.display = "";
            phoneField.style.display = "";
            submitButton.value = "S'inscrire";
        };

        loginTab.onclick();

        return await new Promise(resolve => {
            // If we close the popup, resolve NULL.
            closeButton.onclick = () => {
                modal.remove();
                resolve(null);
            };

            // If we submit the form, there is logic to be done.
            // Required to be launch asynchronously so we can early return 'false'
            // which prevent browser to redirect the user due to form submission.
            const submitForm = async () => {
                const isLogin = loginTab.classList.contains("is-active");
                const email = emailInput.value;
                const password = passwordInput.value;
                const confirmPassword = passwordConfirmInput.value;
                const username = usernameInput.value;
                const phone = phoneInput.value;

                if (isLogin) {
                    // Try to login the user, may throw in cade of error.
                    let result;
                    try {
                        result = await this.login(email, password);
                    } catch(error) {
                        errorText.innerText = error;
                        errorNotif.style.display = "";
                        return;
                    }
                    if (result) {
                        modal.remove();
                        resolve(result);
                    } else {
                        errorText.innerText = "Impossible de se connecter : courriel et mot de passe incorrect ?";
                        errorNotif.style.display = "";
                    }
                }
                else {
                    if (password != confirmPassword) {
                        errorText.innerText = "Les mots de passe ne correspondent pas.";
                        errorNotif.style.display = "";
                    } else {
                        // Try to create a new account, may throw in cade of error.
                        let result;
                        try {
                            result = await this.create(email, password, username, phone);
                        } catch(error) {
                            errorText.innerText = error;
                            errorNotif.style.display = "";
                            return;
                        }
                        if (result) {
                            modal.remove();
                            resolve(result);
                        } else {
                            errorText.innerText = "Impossible de créer un compte.";
                            errorNotif.style.display = "";
                        }
                    }
                }
            };

            form.onsubmit = () => { submitForm(); return false; };
        });
    },

    logout() {
        // If we are not connected, do nothing.
        if (!this._value)
            return;
        // Ask the server to logout the user, invalidating its session token.
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "/api/logout", true);
        xhr.send(this._value.token);
        this._value = null;
        localStorage.removeItem("user");
        window.location = "";
    }
}