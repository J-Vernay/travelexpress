
// Object responsible to cache Towns in localStorage.
// This is done to prevent searching towns from network in every page
// (about 68ko of plain JSON).

const Towns = {
    _value: null,   // _value[province][city] -> id

    async get() {
        // If already cached, return it.
        if (this._value) return this._value;

        // If 'towns' is not in localStorage, ensure cachetime is not either.
        if (!localStorage.getItem("towns"))
            localStorage.removeItem("towns_cachetime");
        
        // Prepare request. If there is data in cache, specify cachetime.
        let cachetime = localStorage.getItem("towns_cache");
        let query = cachetime ? `/api/towns?cachetime=${cachetime}`: "/api/towns";
        
        // Send request with GET method.
        let xhr = new XMLHttpRequest();
        xhr.open("GET", query, true);

        return await new Promise(resolve => {
            xhr.onload = () => {
                if (xhr.responseText != "{}") {
                    // Data was sent, update localStorage and cache.
                    localStorage.setItem("towns", xhr.responseText);
                    this._value = JSON.parse(xhr.responseText);
                }
                // Update cachetime with the current time.
                localStorage.setItem("towns_cachetime", Date.now() / 1000);
                resolve(this._value);
            };
            xhr.send();
        });
    },
}