# Documentation de TravelExpress

TravelExpress est une fausse application web réalisée par Julien Vernay
pour le cours de Systèmes Répartis dispensé à l'UQAC en 2021.
Cette page recense la documentation disponible sur ce projet.

# Déploiement

TravelExpress doit d'abord être compilé pour le serveur. CMake est utilisé pour cela.
Une fois CMake et un compilateur C installé, il faut faire :
```sh
mkdir build     # Création du dossier où sera placé l'exécutable.
cd build        # Aller dans le dossier 'build'.
cmake ..        # Depuis le dossier 'build' configuration de CMake.
cmake --build . # Compilation du projet.
cd ..           # Retour au dossier parent, la racine du projet.
```

Une fois le projet compilé, il est nécessaire de lancer l'exécutable depuis la racine.
Il faut tout d'abord initialiser la base de données en faisant :
```sh
rm -f travelexpress.sqlite3      # Supprimer la base de données si elle existe déjà.
./build/travelexpress create-db  # Recréer une base de données neuve.
```
Cette opération peut prendre un certain temps, correspondant aux insertions des villes
dans la base de données.

Ensuite, il suffit de lancer le projet sans argument, ou avec un numéro de port :
```sh
./build/travelexpress 8000    # Serveur HTTP sur le port 8000
```
On peut accéder au site sur navigateur à l'adresse [http://localhost:8000/](http://localhost:8000/).
Le serveur ne gère pas HTTPS par lui-même. Par sécurité et pour éviter que des mots de passe
en clair soient échangés sur le réseau, le serveur n'écoute que sur localhost, empêchant les accès distants.  
Pour déployer l'application sur le Web, il est donc nécessaire d'utiliser un reverse-proxy servant
de terminateur SSL, et faisant le pont entre l'extérieur et le backend de travelexpress.  
Par exemple, nginx permet de le faire.

# État du prototype

Ayant sous-estimé la durée de ce projet en étant seul, je n'ai pas pu réaliser toutes les fonctionnalités demandées.
En l'état, l'application permet de s'inscrire, se connecter, poster des trajets et en chercher.
Cependant, elle ne permet pas de réserver une place dans un trajet.

Techniquement, au vu des fonctionnalités déjà implémentées, ce qui manque est implémentable "facilement"
(l'architecture et l'organisation du code étant correcte, la base de donnée déjà prête, etc).
C'est donc bien le temps qui m'empêche de développer la réservation de trajet, surtout du côté
front-end qui est très chronophage pour obtenir quelque chose de fonctionnel et graphiquement décent.

Pour implémenter la réservation, voici les étapes que j'ai prévu mais n'ai pas pu réaliser dû au temps :
1. Ajouter une table dans la base de données `Reservation(userid, travelid, date, which)`,
   avec `which` représentant si c'est l'aller ou le retour ou les deux qui sont réservés,
   et `date` la date de réservation (nécessaire si le trajet est régulier pour savoir quel jour est occupé)
2. Ajouter une nouvelle API `api/reserve?travel={}&date={}&which={} | POST: token` qui ajoute une
   entrée dans la table `Reservation` sous condition que l'utilisateur a assez de crédits.
3. Ajouter un formulaire de rechargement de crédits, affiché en cas de réservation sans assez de crédits.
4. Ajouter sur la page de recherche un bouton de réservation pour chaque trajet affiché, appelant
   lanouvelle API.
5. Dans `api/travels` (qui retourne les trajets d'un utilisateur), ajouter les coordonnées des utilisateurs
   ayant réservés chaque trajet.
6. Ajouter une API `api/reservations` (qui retourne les trajets réservés d'un utilisateur), 
   avec les coordonnées du conducteur.
7. Ajouter une page "Mes réservations" pour voir les prochains trajets à venir que l'utilisateur a réservé.

# Architecture

TravelExpress est une application à deux partis, le client et le serveur.
Il n'y a pas d'entité séparée pour la base de donnée : cette dernière est
complètement intégrée dans la partie serveur.

Dans le dossier `www/`, on trouve les fichiers envoyés "tel quel" au client.
Il y a cependant une subtilité : quand le serveur sert un fichier HTML au client,
il rajoute au début et à la fin respectivement les fichiers `_prefix.html` et `_suffix.html`.
Ceci est fait afin d'éviter de dupliquer le code redondant (DOCTYPE, style, barre de navigation...).

Dans le dossier `data/`, on trouve les données utiles à l'application, qui sont les villes et leurs provinces. `cities.txt` présente une ville par ligne, et a été récupéré sur le site
[http://www.canada-city.ca/all-cities-in-canada.php](canada-city.ca).

La partie serveur est codée en langage C. Ce choix n'est pas motivé par une quelconque raison technique
mais purement pour l'envie de réaliser une application non-triviale dans ce langage.  
Elle a deux rôles : servir les fichiers statiques du dossier `www/`,
et proposer une interface REST sur la base de donnée SQL.

La partie client est codée en Javascript. En plus d'ajouter quelques fonctionnalités au formulaire,
son usage principal est d'appeler les interfaces REST pour récupérer les données et les afficher à l'utilisateur. Autrement dit, tout le rendu des pages est réalisé côté client, le serveur n'étant pas
responsable du dynamisme des pages.

Les requêtes HTML sont donc soit des fichiers statiques, soit des API:
- `/api/...?...` : requêtes API, retournent du JSON
- `/...` (autre): requêtes statiques, HTML, CSS, JS, images...

La documentation des API disponibles est située dans `/src/api/api.h`.

# Implémentation

La partie serveur est codée en C, avec trois bibliothèques externes (dans le dossier `deps/`):
- SQLite3 pour la base de données : il s'agit d'une bibliothèque et non d'un serveur comme MySQL ou PostgreSQL : la partie base de données est donc comprise dans l'application.
- Mongoose pour le serveur HTTP : bibliothèque faisant abstraction de la partie réseau.
- Sha3 pour le hachage des mots de passe ( https://github.com/brainhub/SHA3IUF )

Les sources sont situées dans le dossier `/src`. Le sous-dossier `/api` est dédié aux réponses des requêtes API.


