

/// Helper function to create a hierarchy of HTML elements.
const createElem = (tagName, className, parent, innerText) => {
    const elem = document.createElement(tagName);
    elem.className = className;
    if (parent) parent.appendChild(elem);
    if (innerText) elem.innerText = innerText;
    return elem;
};

/// Create an entry for each travel given each caracteristic separately.
const createTravelEntryImpl = (from, dest, freq_date, hour, passengers, maxpassengers) => {
    const root = createElem("div", "columns");
    const left = createElem("div", "column has-text-centered", root);
    createElem("h2", "", left, `${from} ⇒ ${dest}`);
    createElem("p", "", left, freq_date);
    createElem("p", "", left, hour);
    const right = createElem("div", "column has-text-centered", root);
    createElem("h2", "", right, `Passagers : ${passengers.length}/${maxpassengers}`);
    for (const passenger of passengers)
        createElem("p", "", right, passenger);
    return root;
};

let towns_id = [];
/// Populate towns_id [town_id -> town_name] given "towns" object previously
/// obtained with Towns.get() .
const loadTownsId = (towns) => {
    // Constructing an array [town_id -> town_name]
    for (const [_,province] of Object.entries(towns))
        for (const [town, id] of Object.entries(province))
            towns_id[id] = town;
};

/// Create entry from given travel object.
const createTravelEntry = (travel, parent) => {
    // Get textual, user-friendly representation of every field.

    const from = towns_id[travel.from];
    const dest = towns_id[travel.dest];

    let freq_date = "";
    if (travel.regular) {
        const daynames = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
        for (let i = 0; i < 7; ++i) {
            if (travel.regular[i] == 'x')
                freq_date += `${daynames[i]} - `;
        }
        freq_date = freq_date.slice(0, -3); // removing last " - ".
    } else if (travel.punctual) {
        freq_date = travel.punctual;
    }
    const hour = `${Math.floor(travel.hour / 100)}:${travel.hour % 100}`;
    
    // Create a bounding box for the element.
    const box = createElem("div", "box", parent);
    // Insert the new travel entry in this box.
    if (!travel.passengers) {
        travel.passengers = new Array(travel.passengercount);
    }
    box.appendChild(createTravelEntryImpl(from, dest, freq_date, hour,
                        travel.passengers, travel.maxpassengers));
};