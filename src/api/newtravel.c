#include "api.h"
#include "mongoose.h"

#include <inttypes.h>

/// Creates a new travel with the given informations:
/// API:
///    [POST] /api/newtravel?from={id}&dest={id}&passengers={nb}
///               &punctual={yyyy-MM-dd} # if punctual travel, one-time
///               &regular={xx-x--x}     # if regular travel, each 'x' is a day of week when the travel is occuring.
///               &hour={HHmm}
///               &return={HHmm}         # if this is a roundtrip travel
///           : [token]
void api_newtravel(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db) {

    // Parse query arguments.

    int64_t from_id = 0, dest_id = 0, passengers = 0, hour = 0, returnhour = 0;
    bool is_punctual = false, is_regular = false, is_roundtrip = false;
    struct mg_str punctual = {0}, regular = {0};

    struct mg_str query = msg->query, name, value;
    while (query_iterate(&query, &name, &value)) {
        if (mg_vcmp(&name, "from") == 0)
            from_id = atoll(value.ptr);
        else if (mg_vcmp(&name, "dest") == 0)
            dest_id = atoll(value.ptr);
        else if (mg_vcmp(&name, "passengers") == 0)
            passengers = atoll(value.ptr);
        else if (mg_vcmp(&name, "punctual") == 0)
            is_punctual = true, punctual = value;
        else if (mg_vcmp(&name, "regular") == 0)
            is_regular = true, regular = value;
        else if (mg_vcmp(&name, "hour") == 0)
            hour = atoll(value.ptr);
        else if (mg_vcmp(&name, "return") == 0)
            is_roundtrip = true, returnhour = atoll(value.ptr);
        else {
            mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Paramètre '%.*s' invalide.\"}",
                name.len, name.ptr);
            return;
        }
    }

    // Validating data.

    char const* error = NULL;
    if (from_id == 0 || dest_id == 0)
        error = "Ville de départ et d'arrivée non spécifiée.";
    else if (is_punctual && is_regular)
        error = "'regular' et 'punctual' sont exclusifs.";
    else if (!is_punctual && !is_regular)
        error = "Une date 'regular' ou 'punctual' doit être spécifiée.";
    else if (passengers < 1 || passengers > 20) 
        error = "Le nombre de passagers doit être compris entre 1 et 20.";
    else if (hour % 100 >= 60 || hour >= 2400)
        error = "Format invalide pour l'heure de départ (HHmm attendu).";
    else if (is_roundtrip && (returnhour % 100 >= 60 || returnhour >= 2400))
        error = "Format invalide pour l'heure de retour (HHmm attendu).";
    else if (is_punctual && punctual.len == 0)
        error = "Date du trajet manquante.";
    else if (is_punctual && punctual.len != 10)
        error = "Format invalide pour la date (yyyy-MM-dd attendu).";
    else if (is_regular && regular.len != 7)
        error = "Format invalide pour la fréquence (xx-x-x- attendu).";
    else if (is_regular && mg_vcmp(&regular, "-------") == 0)
        error = "Aucun jour n'a été spécifié pour le trajet.";
    
    // Get the user responsible for the request, with the token provided in msg->body .

    int64_t userid;
    if (!error) {
        sqlite3_stmt* stmt;
        sqlite3_prepare_v2(db, "SELECT userid FROM Session WHERE id=?;", -1, &stmt, NULL);
        sqlite3_bind_blob(stmt, 1, msg->body.ptr, msg->body.len, SQLITE_STATIC);
        if (sqlite3_step(stmt) != SQLITE_ROW)
            error = "Session invalide.";
        else
            userid = sqlite3_column_int64(stmt, 0);
        sqlite3_finalize(stmt);
    }

    if (error) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"%s\"}", error);
        return;
    }

    // Insert travel data into database.

    sqlite3_stmt* stmt;
    sqlite3_prepare_v2(db,
        "INSERT INTO Travel(userid, from_, dest, passengers, hour, return, punctual, regular)"
        "VALUES(?,?,?,?,?,?,?,?);", -1, &stmt, NULL);
    sqlite3_bind_int64(stmt, 1, userid);
    sqlite3_bind_int64(stmt, 2, from_id);
    sqlite3_bind_int64(stmt, 3, dest_id);
    sqlite3_bind_int64(stmt, 4, passengers);
    sqlite3_bind_int64(stmt, 5, hour);
    if (is_roundtrip)
        sqlite3_bind_int64(stmt, 6, returnhour);
    else
        sqlite3_bind_null(stmt, 6);
    if (is_punctual) {
        sqlite3_bind_text(stmt, 7, punctual.ptr, punctual.len, SQLITE_STATIC);
        sqlite3_bind_null(stmt, 8);
    } else {
        sqlite3_bind_null(stmt, 7);
        sqlite3_bind_text(stmt, 8, regular.ptr, regular.len, SQLITE_STATIC);
    }

    if (sqlite3_step(stmt) != SQLITE_DONE) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Création du trajet impossible : %s\"}",
            sqlite3_errmsg(db));
        return;
    }

    // Output an empty dictionnary to confirm creation.

    mg_http_reply(c, 400, HEADER_JSON, "{}");
}