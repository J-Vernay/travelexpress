
#include <stdlib.h>

#include "api/api.h"

/// Implemented in create-db.c
void createDatabase();
/// Implemented in serve-http.c
bool initHtmlPrefixSuffix();
void freeHtmlPrefixSuffix();
void serveHttp(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db);


/// Called by Mongoose every time an event occurs for a given connection.
/// An event is for example \c MG_EV_HTTP_MSG when an HTTP message is received.
static void handleConnectionEvent(struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
    // We have received an HTTP request.
    if (ev == MG_EV_HTTP_MSG) {
        struct mg_http_message* msg = (struct mg_http_message*)ev_data;
        sqlite3* db = (sqlite3*)fn_data;
        serveHttp(c, msg, db);
    }
}

/// Usage: ./travelexpress [port=8000]
///     OR ./travelexpress create-db
int main(int argc, char** argv) {
    int port = 8000;
    if (argc >= 2) {
        printf("argv %s\n", argv[1]);
        if (strcmp(argv[1], "create-db") == 0) {
            createDatabase();
            return 0;
        } else {
            port = atoi(argv[1]);
        }
    }

    // Open database.
    sqlite3* db;
    if (sqlite3_open("travelexpress.sqlite3", &db) != SQLITE_OK) {
        fputs("Could not open database.\n", stderr);
        sqlite3_close(db);
    }

    
    if (!initHtmlPrefixSuffix()) {
        sqlite3_close(db);
        return EXIT_FAILURE;
    }

    // Creating URL "http://localhost:PORT".
    // Can only serve localhost for security reason, as it is not secure.
    // May be used behind a reverse-proxy which acts as a TLS termination,
    // such as nginx.
    char* server_url = NULL;
    mg_asprintf(&server_url, 0, "http://localhost:%d", port);

    // Initializes Mongoose, the library used for network and HTTP.
    struct mg_mgr mongoose_manager;
    mg_mgr_init(&mongoose_manager);

    // Listen HTTP requests Last argument is 'db', which is retrieved
    // in 'fn_data' argument of 'handleConnectionEvent'.
    struct mg_connection* server_connection =
        mg_http_listen(&mongoose_manager, server_url, &handleConnectionEvent, db);
    // URL not needed anymore.
    free(server_url);

    if (server_connection == NULL) {
        mg_mgr_free(&mongoose_manager);
        freeHtmlPrefixSuffix();
        sqlite3_close(db);
        fputs("Could not create connection for HTTP server.\n", stderr);
        return EXIT_FAILURE;
    }

    // Event loop, which calls 'handleConnectionEvent' for each event.
    for(;;)
        mg_mgr_poll(&mongoose_manager, 1000);

    // End of program.
    freeHtmlPrefixSuffix();
    mg_mgr_free(&mongoose_manager);
    sqlite3_close(db);
    return EXIT_SUCCESS;
}