#include "api.h"

/// Retrieves a list of travels given wanted date, depart and arrival destination.
/// Returns a JSON list of { id, from, dest, hour, [return, ] (punctual|regular), passengercount, maxpassengers }
/// API:
///    [GET] /api/search?from={id}&to={id}&date={yyyy-MM-dd}
void api_search(struct mg_connection* c, struct mg_http_message* msg, sqlite3* db) {
    int64_t from = 0, dest = 0;
    struct mg_str date = {0}, query = msg->query, name, value;
    while (query_iterate(&query, &name, &value)) {
        if (mg_vcmp(&name, "from") == 0)
            from = atoll(value.ptr);
        else if (mg_vcmp(&name, "dest") == 0)
            dest = atoll(value.ptr);
        else if (mg_vcmp(&name, "date") == 0)
            date = value;
    }
    if (!from || !dest || !date.ptr || date.len != 10) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Attendu: ?from={id}&to={id}&date={yyyy-MM-dd}\"}");
        return;
    }

    // Get the day of week for the given 'date', used for regular travel searching.
    struct tm time = {0};
    int nb_read;
    sscanf(date.ptr, "%d-%d-%d%n", &time.tm_year, &time.tm_mon, &time.tm_mday, &nb_read);
    if (nb_read != 10) {
        mg_http_reply(c, 400, HEADER_JSON, "{\"error\": \"Format de date invalide, attendu: yyyy-MM-dd .\"}");
        return;
    }
    time.tm_year -= 1900; // 1900-indexed
    time.tm_mon -= 1; // 0-indexed
    mktime(&time);
    // time.tm_wday contains the day of week [0;6] = [Sunday;Saturday]
    // In our string (indexed base-1 in SQL), index 1 is Monday and index 7 is Sunday: we need to modify.
    int weekday = time.tm_wday == 0 ? 7 : time.tm_wday;

    
    // Construct a JSON array in 'result'.

    sqlite3_str* result = sqlite3_str_new(db);
    sqlite3_str_appendall(result, "[ ");

    // For each punctual travel matching the date:
    char const* sql_query =
        "SELECT * FROM Travel WHERE from_ = ? AND dest = ? AND"
            "(punctual = ? OR SUBSTRING(regular, ?, 1) = 'x');";

    sqlite3_stmt* stmt;
    sqlite3_prepare_v2(db, sql_query, -1, &stmt, NULL);
    sqlite3_bind_int64(stmt, 1, from);
    sqlite3_bind_int64(stmt, 2, dest);
    sqlite3_bind_text(stmt, 3, date.ptr, date.len, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 4, weekday);
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        // Travel(id, userid, from_, dest, passengers, hour, return, punctual, regular)

        sqlite3_str_appendf(result,
            "{ \"id\": %"PRId64", \"from\": %"PRId64", \"dest\": %"PRId64", \"hour\": %"PRId64", ",
            sqlite3_column_int64(stmt, 0),
            sqlite3_column_int64(stmt, 2),
            sqlite3_column_int64(stmt, 3),
            sqlite3_column_int64(stmt, 5));
        
        // Is it roundtrip = is column 6 'return' not NULL ?
        if (sqlite3_column_type(stmt, 6) == SQLITE_INTEGER)
            sqlite3_str_appendf(result, "\"return\": %"PRId64", ",
                sqlite3_column_int64(stmt, 6));
        
        // Is it punctual = is column 7 'punctual' not NULL ?
        if (sqlite3_column_type(stmt, 7) == SQLITE_TEXT)
            sqlite3_str_appendf(result, "\"punctual\": \"%.*s\", ",
                sqlite3_column_bytes(stmt, 7),
                sqlite3_column_text(stmt, 7));
        
        // Is it regular = is column 8 'regular' not NULL ?
        if (sqlite3_column_type(stmt, 8) == SQLITE_TEXT)
            sqlite3_str_appendf(result, "\"regular\": \"%.*s\", ",
                sqlite3_column_bytes(stmt, 8),
                sqlite3_column_text(stmt, 8));
        
        sqlite3_str_appendf(result, "\"maxpassengers\": %"PRId64", ",
            sqlite3_column_int64(stmt, 4));
        
        // TODO: Make database calls to get count of passengers which have reserved this travel.
        sqlite3_str_appendall(result, "\"passengercount\": 0 ");
        
        sqlite3_str_appendall(result, "},");
    }
    
    // replacing trailing comma with ']' to end the array.
    char* result_str = sqlite3_str_value(result);
    int result_size = sqlite3_str_length(result);
    result_str[result_size-1] = ']';

    mg_http_reply(c, 200, HEADER_JSON, "%s", sqlite3_str_value(result));

    sqlite3_str_finish(result);

}
